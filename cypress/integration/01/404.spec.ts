/// <reference types="Cypress" />

import { login } from "../../support/helpers";
import { notFoundPage, homePage } from "../../../dom/index";
import { Routes } from "../../support/routes";

describe("Check 404 page", () => {
  it("should display proper elements for logged in user", () => {
    login("admin", Routes.notFound);
    cy.get(notFoundPage.title).should("to.have.text", "404");
    cy.get(notFoundPage.subtitle).should("to.have.text", "Not Found!");
    cy.get(notFoundPage.text).should("to.have.text", "Looks like that page is gone...");
    cy.get(notFoundPage.button).should("to.have.text", "Take me home");
  });

  it("should redirect to 404 page for broken link", () => {
    login("admin", "brokenurl");
    cy.get(notFoundPage.title).should("to.be.visible");
  });

  it("should redirect to home for broken link if user not logged in", function() {
    cy.get(homePage.button).should("to.be.visible");
  });
});