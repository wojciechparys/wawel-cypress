/// <reference types="Cypress" />

import { Routes } from "../../support/routes";
import { login } from "../../support/helpers";
import { forEach } from "lodash";

const categoriesList = require("../../../database/categories.json") as { isApplicable: boolean, name: string }[];
const regionsList = require("../../../database/regions.json") as string[];

import { notFoundPage, page, pageTitle, programCategoriesPreview, View } from "../../../dom";
import { programRegionsPreview } from "../../../dom/elements";

describe("Check correct access to different views", () => {


  const appID: any = {
    preview: "673wMakv1YRSccrTvYXFA4wFc9R2",
    productInterview: "9z3ProductInterview",
    communityInterview: "9z2CommunityInterview"
  };

  const links: View.List | any = {
    applicationList: Routes.applicationDashboard,
    applicationPreview: Routes.applicationPreview + appID.preview,
    communityInterview: Routes.communityInterview + appID.communityInterview,
    productInterview: Routes.productInterview + appID.productInterview,
    eligibilityCheckRegionalLead: Routes.eligibilityCheck + "lead/" + appID.preview,
    eligibilityCheckPoC: Routes.eligibilityCheck + "poc/" + appID.preview,
    applicationDetails: Routes.applicationDetails + appID.preview,
    applicationLogs: Routes.applicationDetails + appID.preview + "/logs",
    managingUsers: Routes.managingUsers,
    managingUsersPreview: Routes.managingUsers + "/0",
    managingUsersEdit: Routes.managingUsers + "/0/edit",
    programCategories: Routes.programCategories,
    programCategoryPreview: Routes.programCategories + "/0",
    programRegions: Routes.programRegions,
    programRegionPreview: Routes.programRegions + "/0"
  };

  const adminView: View.List = {
    applicationList: true,
    applicationPreview: true,
    communityInterview: false,
    productInterview: false,
    eligibilityCheckRegionalLead: false,
    eligibilityCheckPoC: false,
    applicationDetails: true,
    applicationLogs: true,
    managingUsers: true,
    managingUsersPreview: true,
    managingUsersEdit: true,
    programCategories: true,
    programCategoryPreview: true,
    programRegions: true,
    programRegionPreview: true
  };
  const userView: View.List = {
    applicationList: false,
    applicationPreview: false,
    communityInterview: false,
    productInterview: false,
    eligibilityCheckRegionalLead: false,
    eligibilityCheckPoC: false,
    applicationDetails: false,
    applicationLogs: false,
    managingUsers: false,
    managingUsersPreview: false,
    managingUsersEdit: false,
    programCategories: false,
    programCategoryPreview: false,
    programRegions: false,
    programRegionPreview: false
  };
  const regionalLeadView: View.List = {
    applicationList: true,
    applicationPreview: true,
    communityInterview: false,
    productInterview: false,
    eligibilityCheckRegionalLead: true,
    eligibilityCheckPoC: false,
    applicationDetails: true, //@todo readonly
    applicationLogs: true,
    managingUsers: true, //@todo readonly,
    managingUsersPreview: true,
    managingUsersEdit: false,
    programCategories: true,
    programCategoryPreview: true,
    programRegions: true,
    programRegionPreview: true
  };
  const pocView: View.List = {
    applicationList: true,
    applicationPreview: true,
    communityInterview: false,
    productInterview: false,
    eligibilityCheckRegionalLead: false,
    eligibilityCheckPoC: true,
    applicationDetails: true,
    applicationLogs: true,
    managingUsers: true, //@todo readonly
    managingUsersPreview: true,
    managingUsersEdit: false,
    programCategories: true,
    programCategoryPreview: true,
    programRegions: true,
    programRegionPreview: true
  };
  const communityView: View.List = {
    applicationList: true,
    applicationPreview: true,
    communityInterview: true,
    productInterview: false,
    eligibilityCheckRegionalLead: false,
    eligibilityCheckPoC: false,
    applicationDetails: true, //@todo readonly
    applicationLogs: true,
    managingUsers: false,
    managingUsersPreview: false,
    managingUsersEdit: false,
    programCategories: false,
    programCategoryPreview: false,
    programRegions: false,
    programRegionPreview: false
  };
  const productView: View.List = {
    applicationList: true,
    applicationPreview: true,
    communityInterview: false,
    productInterview: true,
    eligibilityCheckRegionalLead: false,
    eligibilityCheckPoC: false,
    applicationDetails: true, //@todo readonly
    applicationLogs: true,
    managingUsers: true, //@todo readonly
    managingUsersPreview: true,
    managingUsersEdit: false,
    programCategories: true,
    programCategoryPreview: true,
    programRegions: true,
    programRegionPreview: true
  };
  const googleView: View.List = {
    applicationList: true,
    applicationPreview: true,
    communityInterview: false,
    productInterview: false,
    eligibilityCheckRegionalLead: false,
    eligibilityCheckPoC: false,
    applicationDetails: true, //@todo readonly
    applicationLogs: true,
    managingUsers: true, //@todo readonly,
    managingUsersPreview: true,
    managingUsersEdit: false,
    programCategories: true,
    programCategoryPreview: true,
    programRegions: true,
    programRegionPreview: true
  };
  const expertView: View.List = {
    applicationList: false,
    applicationPreview: false,
    communityInterview: false,
    productInterview: false,
    eligibilityCheckRegionalLead: false,
    eligibilityCheckPoC: false,
    applicationDetails: false,
    applicationLogs: false,
    managingUsers: false,
    managingUsersPreview: false,
    managingUsersEdit: false,
    programCategories: false,
    programCategoryPreview: false,
    programRegions: false,
    programRegionPreview: false
  };

  const views: View.Interface = {
    admin: adminView,
    user: userView,
    regionalLead: regionalLeadView,
    poc: pocView,
    community: communityView,
    product: productView,
    google: googleView,
    expert: expertView
  };

  forEach(views, (viewList, user) => {
    describe("Check access for " + user, () => {
      forEach(viewList, (access, pageKey) => {

        it("should display correct view for " + pageKey, function() {
          login(user, links[pageKey]);

          if (access) {
            cy.get(page.title, { timeout: 60000 }).should("contain", pageTitle[pageKey]);

            if (pageKey === "programCategoryPreview") {
              cy.get(programCategoriesPreview.catTitle).should("contain", categoriesList[0].name);
            }

            if (pageKey === "programRegionPreview") {
              cy.get(programRegionsPreview.catTitle).should("contain", regionsList[0]);
            }

          } else {
            cy.get(notFoundPage.title, { timeout: 60000 }).should("to.be.visible");
          }
        });
      });
    });
  });


});