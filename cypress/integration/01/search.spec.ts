/// <reference types="Cypress" />

import { login } from "../../support/helpers";
import { applicationListPage } from "../../../dom/index";
import { Routes } from "../../support/routes";

describe("Check application dashboard search", () => {
  before(() => {
    login("admin", Routes.applicationDashboard);
    cy.get("#mat-tab-label-0-1", { timeout: 60000 }).click();
  });

  it("should search for proper values", function() {
    const multi = [
      ["email", "community.interview@gdeapp_status.com"],
      ["first", "Swap"],
      ["last", "Category"],
      ["firstAndLast", "Swap Category"]
    ];


    for (let i = 0; i < multi.length; i++) {
      cy.searchList(multi[i][1]);
      cy.url().should("contain", encodeURI(multi[i][1]));

      switch (multi[i][0]) {
        case "email":
          cy.get(applicationListPage.firstNameApplicant).should("contain", "Community");
          break;

        case "first":
          cy.get(applicationListPage.firstNameApplicant).should("contain", multi[i][1]);
          break;

        case "last":
          cy.get(applicationListPage.lastNameApplicant).should("contain", multi[i][1]);
          break;

        case "firstAndLast":
          cy.get(applicationListPage.firstNameApplicant).should("contain", "Swap");
          cy.get(applicationListPage.lastNameApplicant).should("contain", "Category");
          break;
      }
    }
  });

});