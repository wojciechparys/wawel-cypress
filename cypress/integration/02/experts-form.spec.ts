/// <reference types="Cypress" />


import { login } from "../../support/helpers";
import { Routes } from "../../support/routes";
import { app, page } from "../../../dom";

const importApplications = require("../../../database/applications.json");
import { environment } from "../../../environments/environment_resolver";
import { pageTitle } from "../../../dom/helpers";

xdescribe("Check experts form submission", () => {
  const app: app = importApplications["673wMakv1YRSccrTvYXFA4wFc9R2"];
  const applicationId: string = environment.users.userAuthValue["uid"];

  describe("Test submitting application", () => {
    it("should open experts form", function() {
      login("user", Routes.onboarding);
      cy.get(page.title, { timeout: 60000 }).should("contain", pageTitle.expertsForm);
    });


  });

});