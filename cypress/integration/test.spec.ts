/// <reference types="Cypress" />

import { firebaseService } from "../support/helpers";

describe("Check firebase data", () => {


  it("should works", function() {
    firebaseService.getActivities().then(apps => {
      expect(apps[0]).to.have.any.keys("email", "category");
    });
  });
});