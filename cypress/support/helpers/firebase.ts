import { environment } from "../../../environments/environment_resolver";

export class FirebaseService {
  databaseURL = environment.db;

  async getApplication(appId?: string) {
    const response = await fetch(`${this.databaseURL}/applications/${appId}.json`);
    return response.json();
  }

  async getActivities() {
    const response = await fetch(`${this.databaseURL}/activities.json`);
    return response.json();
  }

}

export const firebaseService = new FirebaseService();