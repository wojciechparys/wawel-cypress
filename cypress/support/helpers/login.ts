/// <reference types="Cypress" />

import { environment } from "../../../environments/environment_resolver";
import { notFoundPage } from "../../../dom";
import { Routes } from "../routes";

export function login(username: string, route?: string) {
  const authKey = environment.users[username + "AuthKey"];
  const authValue = JSON.stringify(environment.users[username + "AuthValue"]);
  console.log("Login as " + username);

  if (route) {
    cy.visit(route, { onBeforeLoad: () => window.localStorage.setItem(authKey, authValue) });
  } else {
    cy.visit(Routes.notFound, { onBeforeLoad: () => window.localStorage.setItem(authKey, authValue) });
    cy.get(notFoundPage.title, { timeout: 60000 }).should("to.be.visible");
  }

}