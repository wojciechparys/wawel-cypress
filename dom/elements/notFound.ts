export const notFoundPage = {
  title: '.warn .warn__title',
  subtitle: '.warn .warn__text:nth-of-type(1)',
  text: '.warn .warn__text:nth-of-type(2)',
  button: '.warn button'
};