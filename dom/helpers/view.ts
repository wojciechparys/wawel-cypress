import { View } from "../models";

export const pageTitle: View.List | any = {
  applicationList: "GDE Applications Dashboard",
  applicationPreview: "GDE Application Form Preview",
  productInterview: "GDE Product Interview",
  communityInterview: "GDE Community Interview",
  eligibilityCheckRegionalLead: "GDE Eligibility Check",
  eligibilityCheckPoC: "GDE Eligibility Check",
  applicationDetails: "GDE Application Details",
  applicationLogs: "GDE Application Logs",
  managingUsers: "GDE Managing Users",
  managingUsersPreview: "GDE Managing Users",
  managingUsersEdit: "GDE Managing Users",
  programCategories: "GDE Program Categories",
  programCategoryPreview: "GDE Program Categories",
  programRegions: "GDE Program Regions",
  programRegionPreview: "GDE Program Regions",
  expertsForm: "'GDE Application Form"
};