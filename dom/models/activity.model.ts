export namespace Activities {
  export enum Type {
    written = 'Written Content',
    video = 'Video / Podcast',
    talk = 'Public Speaking',
    workshop = 'Workshops',
    story = 'Story'
  }

  export enum ImpactLabel {
    written = 'Reads',
    video = 'Views',
    talk = 'People Attended',
    workshop = 'People Trained',
    story = 'People Impacted'
  }

  export interface ItemBase {
    id: string; // MAPPED PROPERTY FROM firebase
    email: string;
    firstName: string; // MAPPED FROM EXPERT PROFILE
    lastName: string; // MAPPED FROM EXPERT PROFILE
    date: number; // DATE - happened when - timestamp
    dateSubmitted: number; // DATE OF ENTRY - timestamp
    description: string // DESCRIPTION
    link: string; // LINK
    title: string; // TITLE
    type: Type; // CATEGORY
    tags: string[]; // PRODUCTS
  }

  export interface Item extends ItemBase {
    category?: string; // GDE PA
    trained?: number; // TRAINED
    reach?: number; // DIRECT REACH or ONLINE REACH
    city?: string;
    country?: string;
    featured?: boolean; // X
    subtype?: string;
  }


  /**
   * key -> value item pairs for all users
   */
  export interface Entities {
    [key: string]: Item
  }

  /**
   * Flat list of table
   */
  export type FlatList = Item[];

  /**
   * Activities table example
   */
  const table: Entities = {
    'activity_key_1': {
      id: 'activity_key_1',
      email: 'foo.bar@example.com',
      firstName: 'Joe',
      lastName: 'Doe',
      date: 123456789,
      dateSubmitted: 123456789,
      title: 'example title',
      description: 'A blog post',
      link: 'https://google.com',
      type: Type.story,
      tags: ['chrome', 'webTechnologies']
    },
    'activity_key_2': {} as Item,
    'activity_key_3': {} as Item
  };
}
