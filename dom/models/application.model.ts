export interface ReviewStatus {
  user: any;
  capacity?: boolean;
  eligibility: boolean;
  note: string;
  questions?: string;
}

export interface AdminFeedback {
  eligibilityCheck?: ReviewStatus;
  communityInterview?: ReviewStatus;
  productInterview?: ReviewStatus;
}

export interface app {
  'activities': {
    'cc': [{
      'cc-about': string,
      'cc-date': string,
      'cc-link': string,
      'cc-name': string,
      'cc-reach': number
    }],
    'pe': [{
      'pe-about': string,
      'pe-date': string,
      'pe-link': string,
      'pe-name': string,
      'pe-reach': number
    }],
    'sc': [{
      'sc-about': string,
      'sc-date': string,
      'sc-link': string,
      'sc-name': string,
      'sc-reach': number
    }],
    'reach-cc': number
    'reach-pe': number
    'user-referrals': string
  },
  'bio': string,
  'category': string,
  'city': string,
  'company': string,
  'region': string,
  'customLink': string,
  'dateSubmitted': number
  'email': string,
  'fb': string,
  'firstName': string,
  'gender': string,
  'github': string,
  'googleId': string,
  'googleRelationship': string,
  'government': string,
  'lastName': string,
  'linkedin': string,
  poc: ReviewStatus,
  communityInterviewer: ReviewStatus,
  productInterviewer: ReviewStatus,
  regionalLead: ReviewStatus,
  adminFeedback: AdminFeedback,
  autoEmailNotifications: boolean
}
