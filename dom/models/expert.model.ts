import {User} from './user.model';
import { Activities } from './activity.model';

export namespace Experts {


  export interface GitHubStats {
    stargazers: number;
    forks: number;
    repositories: number;
    commits: number;
    pullRequests: number;
    issues: number;
  }

  export interface StackExchangeStats {
    label: string,
    reputation?: number,
    goldBadges?: number,
    silverBadges?: number,
    bronzeBadges?: number,
    answers?: number,
    questions?: number,
    peopleReached?: number
  }

  export interface Roles extends User.Roles {
    expert?: {
      isActive: boolean;
      categories?: string[]
    };
  }

  export interface Item extends User.Item {
    roles: Roles;
    city?: string;
    region?: string;
    expertSince?: number;
    isExpert: boolean,

    bio?: string,

    // Social Media
    customLink?: string,
    fb?: string,
    github?: string,
    googleId?: string,
    linkedin?: string,
    medium?: string,
    stackoverflow?: string,
    twitter?: string,
    website?: string,

    // stats
    stackExchangeStats?: {
      [service: string]: StackExchangeStats
    }
    githubStats?: GitHubStats,
    activities?: Activities.FlatList
  }

  export type List = Item[];
}
