export namespace DrawerMenu {

  export interface Menu {
    appDashboard: boolean ;
    programReports: boolean;
    previewForm: boolean;
    expertsDirectory: boolean;
    managingUsers: boolean;
    programCategories: boolean;
    programRegions: boolean;
    userProfile: boolean;
  }

  export interface DataDetails {
    itemsCount: number,
    menuItems: Menu
    initials: string,
    login: string,
    label: string,
  }

  export interface DataItem {
    [key: string]: DataDetails
  }
}
