export namespace User {

  export interface Roles {
    admin?: {
      isActive: boolean
    };
    regionalLead?: {
      isActive?: boolean,
      regions?: string[],
      statistics?: any
    };
    poc?: {
      isActive?: boolean,
      categories?: string[],
      statistics?: any
    };
    productInterviewer?: {
      isActive?: boolean,
      categories?: string[],
      statistics?: any
    };
    communityInterviewer?: {
      isActive?: boolean,
      categories?: string[],
      statistics?: any
    };
  }

  export type Role = keyof Roles;

  export interface Item {
    id?: string;
    email: string;
    roles: Roles;
    activeRoles?: string[], // Mapped property for optimization
    inactive?: boolean;
    firstName?: string;
    lastName?: string;
    expertSince?: number;
    photoURL?: string;
    associations?: any;
    loginTimestamp?: number
  }

  export type List = Item[];
}
