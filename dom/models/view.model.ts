export namespace View {

  export interface List {
    applicationList: boolean | string;
    applicationPreview: boolean | string;
    communityInterview: boolean | string;
    productInterview: boolean | string;
    eligibilityCheckRegionalLead: boolean | string;
    eligibilityCheckPoC: boolean | string;
    applicationDetails: boolean | string;
    applicationLogs: boolean | string;
    managingUsers: boolean | string;
    managingUsersPreview: boolean | string;
    managingUsersEdit: boolean | string;
    programCategories: boolean | string;
    programCategoryPreview: boolean | string;
    programRegions: boolean | string;
    programRegionPreview: boolean | string;
  }

  export interface Interface {
    admin: List;
    user: List;
    regionalLead: List;
    poc: List;
    community: List;
    product: List;
    google: List;
    expert: List;
  }
}