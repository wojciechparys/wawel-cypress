let env: any;
const env_w = require('./environment.wojtek').environment;
const env_e2e = require('./environment.e2e').environment;

if (process.env.npm_config_e) {
    switch (process.env.npm_config_e) {
        case 'wojtek':
            env = env_w;
            break;
        case 'local':
        default:
            env = env_e2e;
    }
} else {
    env = env_e2e;
}

export const environment = env;
